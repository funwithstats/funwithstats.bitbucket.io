function reboundGraph(graph, xMin, xMax, yMin, yMax) {
	graph.clearGraph();
	return setUpGraph(graph.canvas, graph.context, xMin * 1, xMax * 1, yMin * 1, yMax * 1, graph.width, graph.height);
}

function resizeGraph(graph, height, width) {
	return setUpGraph(graph.canvas, graph.context, graph.xMin, graph.xMax, graph.yMin, graph.yMax, width, height);
}

function setUpGraph(canvas, context, xMin, xMax, yMin, yMax, width, height) {

	canvas.height = height;
	canvas.width = width;

	var graphObj = {
		"canvas": canvas,
		"context": context,
		"xMin": xMin,
		"xMax": xMax,
		"yMin": yMin,
		"yMax": yMax,
		"width": width,
		"height": height,
		"yScale": height / (yMax - yMin),
		"xScale": width / (xMax - xMin),
		"getPoint": function (x, y) {
			if (x < this.xMin || x > this.xMax || y < this.yMin || y > this.yMax) {
				//console.log({"X": x, "Y":y});
				//console.log("Outside of graph");
				return null;
			}

			var xCoor = this.xScale * (x - this.xMin);
			var yCoor = this.yScale * (this.yMax - y);

			return { "X": xCoor, "Y": yCoor };

		},
		"drawPoint": function (x, y, size = 5, color = "black") {
			if (x < this.xMin || x > this.xMax || y < this.yMin || y > this.yMax) {
				//console.log("Outside of graph");
				return null;
			}

			var xCoor = this.xScale * (x - this.xMin);
			var yCoor = this.yScale * (this.yMax - y);

			this.context.fillStyle = color;
			this.context.strokeStyle = color;
			this.context.beginPath();
			this.context.arc(xCoor, yCoor, size, 0, 2 * Math.PI, true);
			this.context.stroke();
			this.context.fill();

			this.context.fillStyle = "black";
			this.context.strokeStyle = "black";

			return { "X": xCoor, "Y": yCoor };
		},
		"getValidPoint": function (x, y) {
			if (y > this.yMax) {
				return this.getPoint(x, this.yMax);
			} else if (y < this.yMin) {
				return this.getPoint(x, this.yMin);
			}

			return this.getPoint(x, y);

		},

		"drawGraph": function (inputF, variable = "x", axis = "x") {

			this.clearGraph();

			if (axis == "y" || axis == "Y") {

				if (variable != "y") {
					inputF = inputF.replaceAll(variable, "y");
				}

				return this.drawYGraph(inputF);

			}

			if (variable != "x") {
				inputF = inputF.replaceAll(variable, "x");
			}

			return this.drawXGraph(inputF);


		},
		"shadeUnderCurve": function (inputFx, lower, upper, split = false) {
			this.clearGraph();
			this.drawXGraph(inputFx);

			if (split) {
				this.drawXGraph(inputFx, -1000, lower, true);
				this.drawXGraph(inputFx, upper, 1000, true);
			} else {
				this.drawXGraph(inputFx, lower, upper, true);
			}
		},
		"drawXGraph": function (inputFx, lower = "default", upper = "default", shadeBelow = false, color = "black") {

			this.context.fillStyle = color;
			this.context.strokeStyle = color;

			var x;

			if (lower == "default") {
				lower = this.xMin;
			}

			if (lower < this.xMin) {
				lower = this.xMin;
			} else if (lower >= this.xMax) {
				// Don't start drawing until you get to the upper bound.
				return;
			}

			x = lower;

			if (upper == "default") {
				upper = this.xMax;
			}

			if (upper > this.xMax) {
				upper = this.xMax;
			} else if (upper <= this.xMin) {
				// Stop drawing before you get to the lower bound...
				return;
			}


			var y = eval(inputFx.replaceAll("x", x));

			var lastPointNullFlag = true;

			var point = this.getValidPoint(x, y);

			var zeroP = this.getValidPoint(x, 0);

			if (point !== null) {
				if (shadeBelow) {
					//console.log(zeroP);
					this.context.moveTo(zeroP.X, zeroP.Y);
					this.context.lineTo(point.X, point.Y);
				} else {
					this.context.moveTo(point.X, point.Y);
					var lastPointNullFlag = false;
				}
			}

			this.context.beginPath();

			for (var i = Math.floor((x - this.xMin) * this.xScale); i < this.width; i++) {

				x = this.xMin + (i / this.xScale);

				if (x > upper) {
					break;
				}

				y = eval(inputFx.replaceAll("x", x));
				point = this.getValidPoint(x, y);

				if (point === null) {

					if (!shadeBelow && !lastPointNullFlag) {
						this.context.stroke();
					}

					var lastPointNullFlag = true;
					continue;
				} else if (lastPointNullFlag) {

					this.context.moveTo(point.X, point.Y);

					if (!shadeBelow && lastPointNullFlag) {
						this.context.beginPath();
					}

					var lastPointNullFlag = false;
				}

				this.context.lineTo(point.X, point.Y);
			}

			if (shadeBelow) {
				zeroP = this.getValidPoint(x, 0);
				//console.log(zeroP);
				this.context.lineTo(zeroP.X, zeroP.Y);

				// return to original
				zeroP = this.getValidPoint(lower, 0);
				this.context.lineTo(zeroP.X, zeroP.Y);
				this.context.fill();
			}

			if (!shadeBelow && !lastPointNullFlag) {
				this.context.stroke();
			}

			this.context.fillStyle = "black";
			this.context.strokeStyle = "black";

		},
		"drawYGraph": function (inputFy, lower = "default", upper = "default", color = "black") {

			this.context.fillStyle = color;
			this.context.strokeStyle = color;

			var lastPointNullFlag = true;

			var y;

			if (lower == "default") {
				lower = this.yMin;
			}

			if (lower < this.yMin) {
				lower = this.yMin;
			} else if (lower >= this.yMax) {
				// Don't start drawing until you get to the upper bound.
				return;
			}

			y = lower;

			if (upper == "default") {
				upper = this.yMax;
			}

			if (upper > this.yMax) {
				upper = this.yMax;
			} else if (upper <= this.yMin) {
				// Stop drawing before you get to the lower bound...
				return;
			}

			var x = eval(inputFy.replaceAll("y", y));

			var point = this.getPoint(x, y);

			if (point !== null) {
				this.context.moveTo(point.X, point.Y);
				var lastPointNullFlag = false;
			}

			this.context.beginPath();

			for (var i = 1; i < this.height; i++) {

				y = lower + (i / this.yScale);

				if (y > upper) {
					break;
				}

				x = eval(inputFy.replaceAll("y", y));

				point = this.getPoint(x, y);

				if (point === null) {
					var lastPointNullFlag = true;
					continue;
				} else if (lastPointNullFlag) {
					this.context.moveTo(point.X, point.Y);
					var lastPointNullFlag = false;
				}

				this.context.lineTo(point.X, point.Y);
				this.context.stroke();
			}

			this.context.stroke();

			this.context.fillStyle = "black";
			this.context.strokeStyle = "black";

		},
		"clearGraph": function () {
			this.context.clearRect(0, 0, this.width, this.height);
			this.drawYGraph("0");
			this.drawXGraph("0");
		},
	};


	//console.log(graphObj.context);
	// add axises
	graphObj.clearGraph();

	return graphObj;
};