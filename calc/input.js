function createInput(partOfTable = false, startingValue = "") {
	let divObj = {};

	divObj.evaluateCell = function () {
		return this.value;
	}

	divObj.value = startingValue;
	divObj.el = document.createElement("div");
	divObj.input = document.createElement("input");
	divObj.div = document.createElement("div");
	divObj.input.value = startingValue;
	divObj.div.innerText = divObj.evaluateCell();

	divObj.el.classList = ["psudoInput"];
	//divObj.el.appendChild(divObj.input);
	divObj.el.appendChild(divObj.div);
	divObj.startingValue = startingValue;


	return divObj;
}