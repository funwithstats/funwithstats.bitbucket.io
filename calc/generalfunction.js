// https://stackoverflow.com/questions/1144783/how-to-replace-all-occurrences-of-a-string-in-javascript
String.prototype.replaceAll = function (search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

// use in tables that have an "addRow" function to make tab/enter equivalent, and to make new rows as you tab through them.
function checkForTab(input, event, inverted = false) {



	if (event.keyCode == 13 || event.keyCode == 9) {

		event.preventDefault();

		if (event.keyCode == 9) {
			// tab should still be tab even if enter now goes down the rows.
			inverted = false;
		}

		if (event.shiftKey) {

			if (input.parentElement.previousElementSibling !== null && !inverted && input.parentElement.previousElementSibling.children.length > 0) {
				let nextInp = input.parentElement.previousElementSibling.children[0];

				if (nextInp.classList.contains("hide")) {
					checkForTab(nextInp, event, inverted);
				} else {
					nextInp.focus();
				}

				return;
			}

			let prevRow = input.parentElement.parentElement.previousElementSibling;

			if (input.parentElement.parentElement.previousElementSibling !== null && prevRow.children[prevRow.children.length - 1].children.length > 0) {

				let nextInp = null;

				if (inverted) {
					nextInp = prevRow.children[getChildIndex(input.parentElement)].children[0];
				} else {
					nextInp = prevRow.children[prevRow.children.length - 1].children[0];
				}

				if (nextInp.classList.contains("hide")) {
					checkForTab(nextInp, event, inverted);
				} else {
					nextInp.focus();
				}

				return;
			} else if (input.parentElement.parentElement.previousElementSibling !== null && prevRow.children[prevRow.children.length - 2].children.length > 0) {

				let nextInp = null;

				if (inverted) {
					nextInp = prevRow.children[getChildIndex(input.parentElement)].children[0];
				} else {
					nextInp = prevRow.children[prevRow.children.length - 2].children[0];
				}

				if (nextInp.classList.contains("hide")) {
					checkForTab(nextInp, event, inverted);
				} else {
					nextInp.focus();
				}

			}

			return;
		}

		if (input.parentElement.nextElementSibling != null && !inverted && input.parentElement.nextElementSibling.children.length > 0) {
			let nextInp = input.parentElement.nextElementSibling.children[0];

			if (nextInp.classList.contains("hide")) {
				checkForTab(nextInp, event, inverted);
			} else {
				nextInp.focus();
			}

			return;
		}

		if (input.parentElement.parentElement.nextElementSibling == null) {
			addRow(getParentTable(input).id);
		}

		let nextInp = null;

		if (inverted) {
			nextInp = input.parentElement.parentElement.nextElementSibling.children[getChildIndex(input.parentElement)].children[0];
		} else {
			nextInp = input.parentElement.parentElement.nextElementSibling.children[0].children[0];
		}

		if (nextInp.classList.contains("hide")) {
			checkForTab(nextInp, event, inverted);
		} else {
			nextInp.focus();
		}

	}
}

function showDetails() {
	let detailsDiv = document.getElementById("details");

	detailsDiv.classList.add("fullscreen");
	detailsDiv.classList.remove("hide");
	detailsDiv.scroll(0, 0);

}

function hideDetails() {
	let detailsDiv = document.getElementById("details");

	detailsDiv.classList.add("hide");
	detailsDiv.classList.remove("fullscreen");

}

function insertVariables(equation, variableNames, variableValues) {

	for (let i = 0; i < variableNames.length; i++) {
		equation = equation.toUpperCase().replaceAll(variableNames[i], variableValues[i]);
	}

	return equation;
}

Array.prototype.remove = function (trash) {

	while (this.indexOf(trash) > -1) {
		this.splice(this.indexOf(trash), 1);
	}

	return this;

}

function getChildIndex(element) {
	let i = 0;

	let parent = element.parentElement;

	for (let child of parent.children) {

		if (child == element) {
			return i;
		}

		i++;
	}


}

function getParentTable(el) {
	let parent = el.parentElement;

	while (parent.nodeName.toLowerCase() != "table") {
		parent = parent.parentElement;
	}

	return parent;

}