var canvas = document.getElementById("graph1");
var context = canvas.getContext("2d");
var myGraph = setUpGraph(canvas, context, -3, 3, -0.5, 0.5, 500, 500);

const typeSelector = document.getElementById("testType");
const runBtn = document.getElementById("runBtn");
const result = document.getElementById("result");

// set up the required controls
for (var reqControl of document.getElementsByClassName("required")) {
	reqControl.onkeyup = (ev) => {
		runBtn.classList.add("hide");
		for (var reqControl of document.querySelectorAll(".control-section:not(.hide) .required")) {
			if (reqControl.value.length < 1) {
				// don't enable button
				document.getElementById("runBtn").disabled = true;
				return;
			}
		}
		// all visable controls have values... let it run!
		document.getElementById("runBtn").disabled = false;

		test = ev;

		myGraph.clearGraph();
		result.innerText = "";

		// make it run if they hit enter
		if (ev.keyCode == 13) {
			run();
			return;
		}

		runBtn.classList.remove("hide");

	};

	reqControl.onchange = reqControl.onkeyup;
}

function showControls() {
	runBtn.classList.add("hide");
	result.innerText = "";
	myGraph.clearGraph();


	// hide the control sections we don't need
	for (var sec of document.getElementsByClassName("control-section")) {
		sec.classList.add("hide");
	}

	// clear the controls
	for (var reqControl of document.getElementsByClassName("required")) {
		reqControl.value = "";
	}

	// don't let them run it until it is ready
	document.getElementById("runBtn").disabled = true;

	// display the control section we do need
	document.getElementById(typeSelector.value + "-controls").classList.remove("hide")
}

function run() {

	runBtn.classList.add("hide");

	switch (typeSelector.value) {
		case "normal":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)

			let zPercentile = parseFloat(document.getElementById("normal-percentile").value);

			if (zPercentile <= 0 || zPercentile >= 100) {
				result.innerText = "ERROR: Percentile must be a percent greater than 0 and less than 100.";
				return;
			}

			let z = findz(zPercentile);

			myGraph.shadeUnderCurve(getNormalCurve(), -7, z);

			result.innerText = "z = " + z.toFixed(3);
			break;

		case "t":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)

			let tPercentile = parseFloat(document.getElementById("t-percentile").value);
			var degFree = parseFloat(document.getElementById("t-deg").value);

			if (tPercentile <= 0 || tPercentile >= 100) {
				result.innerText = "ERROR: Percentile must be a percent greater than 0 and less than 100.";
				return;
			} else if (degFree < 1) {
				result.innerText = "ERROR: Degrees of freedom must be positive";
			}

			let t = findt(tPercentile, degFree);

			myGraph.shadeUnderCurve(tPDFFunc(degFree), -100, t);

			result.innerText = "t = " + t.toFixed(3);
			break;

		default:
			console.log("ERROR: Operation not found");
	}

}

showControls();