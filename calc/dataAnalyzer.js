const dataEl = document.getElementById("data");
const resultsEl = document.getElementById("results");
const averageEl = document.getElementById("average");
const sdEl = document.getElementById("sd");
const sdPlusEl = document.getElementById("sdplus");
const tbody = document.getElementById("histoData").children[0];
const staticTbody = document.getElementById("staticData");

function editData() {
	resultsEl.classList.add("hide");
	dataEl.classList.remove("hide");
}

function analyzeData() {
	staticTbody.innerHTML = "";

	let data = [];
	let max = null;
	let min = null;
	dataSum = {};


	for (let tr of tbody.children) {

		for (let td of tr.children) {
			if (td.children.length > 0) {
				// the td has an input. Get its value and make it static.

				if (td.children[0].value.length < 1) {
					missingDataError.innerHTML = "All inputs must have data before you continue.";
					return;
				}

				let val = td.children[0].value * 1;
				data.push(val * 1);

				if (dataSum[val] == undefined) {
					dataSum[val] = 1;
				} else {
					dataSum[val]++;
				}

				if (max == null || val > max) {
					max = val;
				}

				if (min == null || val < min) {
					min = val;
				}

			}
		}

	}

	let tr = document.createElement("tr");
	let dataHeader = document.createElement("td");
	let frequencyHeader = document.createElement("td");

	dataHeader.textContent = "Data";
	frequencyHeader.textContent = "Frequency";

	tr.appendChild(dataHeader);
	tr.appendChild(frequencyHeader);

	staticTbody.appendChild(tr);

	for (let d of Object.keys(dataSum).sort(function (x, y) { return x - y; })) {

		let tr = document.createElement("tr");
		let dataTD = document.createElement("td");
		let frequencyTD = document.createElement("td");

		dataTD.textContent = d;
		frequencyTD.textContent = dataSum[d];

		tr.appendChild(dataTD);
		tr.appendChild(frequencyTD);

		staticTbody.appendChild(tr);


	}

	missingDataError.innerHTML = "";

	let dataAverage = average(data);
	let dataSD = sd(data);
	let dataSDPlus = sdPlus(data);

	averageEl.textContent = dataAverage.toFixed(4);
	sdEl.textContent = dataSD.toFixed(4);
	sdPlusEl.textContent = dataSDPlus.toFixed(4);

	dataEl.classList.add("hide");
	resultsEl.classList.remove("hide");
	window.scrollTo(0, 0);

}

function addRow() {
	let rows = tbody.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];

	let newRow = lastRow.cloneNode(true);

	for (let inp of newRow.getElementsByTagName("input")) {
		inp.value = "";
	}

	tbody.appendChild(newRow);
}

function removeRow() {
	let rows = tbody.getElementsByTagName("tr");

	let lastRow = rows[rows.length - 1];

	if (rows.length < 2) {
		alert("You cannot remove the only row of data.");
		return;
	} else if (
		lastRow.getElementsByClassName("xval")[0].value.length < 1
		|| confirm("Would you like to remove the last row of data?")
	) {

		lastRow.remove();
	}

}