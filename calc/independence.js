//tbody
const table = document.getElementById("chidata").children[0];
const resDiv = document.getElementById("dipslayResults");
const tblDiv = document.getElementById("staticTableSpace");
const expDiv = document.getElementById("expectedValTableSpace");
const chiDiv = document.getElementById("chiTableSpace");

const chiDataContainer = document.getElementById("chiDataContainer");
const addRowContainer = document.getElementById("addRowContainer");
const addColContainer = document.getElementById("addColumnContainer");
const addColBtn = document.getElementById("addColBtn");

const totalChiEl = document.getElementById("totalChi");
const degFreeEl = document.getElementById("degFree");
const pchiEl = document.getElementById("pChi");

let canvas = document.getElementById("chiGraph");
let context = canvas.getContext("2d");

let myGraph = setUpGraph(canvas, context, -3, 3, -0.5, 0.5, 500, 500);

centerAddColCont();
centerAddRowCont();

function addRow() {
	let rows = table.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];

	let newRow = lastRow.cloneNode(true);

	for (let inp of newRow.getElementsByTagName("input")) {
		inp.value = "";
	}

	table.appendChild(newRow);
	centerAddColCont();
};

function removeRow() {
	let rows = table.getElementsByTagName("tr");

	let lastRow = rows[rows.length - 1];

	if (rows.length < 4) {
		alert("You cannot perform a test of independence with less than two rows.");
		return;
	} else {

		let dataCellsEmpty = true;

		for (let inp of lastRow.getElementsByTagName("input")) {
			if (inp.value.length > 0) {
				dataCellsEmpty = false;
				break;
			}
		}

		if (dataCellsEmpty || confirm("Are you sure you want to delete a row which contains data?")) {

			lastRow.remove();
		}

	}

	centerAddColCont();

};

function addColumn() {
	let rows = table.getElementsByTagName("tr");

	for (let tr of rows) {
		let tds = tr.children;

		let lastTD = tds[tds.length - 1];
		let newTD = lastTD.cloneNode(true);
		let newInp = newTD.children[0];

		newInp.value = "";

		tr.appendChild(newTD);

	}

	centerAddRowCont();
};

function removeColumn() {

	let rows = table.getElementsByTagName("tr");

	if (rows[0].children.length < 4) {
		alert("You cannot perform a test of independence with less than two columns.");
		return;
	}

	let cellsAreEmpty = true;

	for (let tr of rows) {
		let tds = tr.children;

		let lastTD = tds[tds.length - 1];

		if (lastTD.children[0].value.length > 0) {
			cellsAreEmpty = false;
			break;
		}

	}

	if (!cellsAreEmpty && !confirm("Are you sure you want to delete a column which contains data?")) {
		return;
	}

	for (let tr of rows) {
		let tds = tr.children;

		let lastTD = tds[tds.length - 1];

		lastTD.remove();

	}

	centerAddRowCont();
};

window.onresize = function () {
	centerAddRowCont();
	centerAddColCont();
};

function calculate() {
	let staticTbody = table.cloneNode(true);
	let rows = staticTbody.getElementsByTagName("tr");
	let data = [];
	let rowNum = 0;

	let rowSums = [];
	let columnSums = [];

	for (let i = 0; i < staticTbody.children[0].children.length; i++) {
		columnSums[i] = 0;
	}

	for (let tr of rows) {
		let columnNum = 0;
		let rowSum = 0;

		data[rowNum] = [];

		for (let td of tr.children) {

			let inp = null;

			if (td.children.length > 0) {

				inp = td.children[0];

				if (inp.classList.contains("chidatacell") && inp.value.length < 1) {
					alert("Every cell must have data before you can perform a calculation.");
					return;
				} else if (inp.classList.contains("chidatacell")) {
					rowSum += parseFloat(inp.value);
					columnSums[columnNum] += parseFloat(inp.value);
					data[rowNum].push(inp.value);

				}

				td.innerHTML = inp.value;

			}

			columnNum++;
		}

		rowSums[rowNum] = rowSum;

		let totalTd = document.createElement("td");

		if (rowNum == 0) {
			totalTd.innerText = "Total";
		} else {
			totalTd.innerText = rowSum;
		}

		tr.appendChild(totalTd);

		rowNum++;
	}

	let totalsRow = document.createElement("tr");

	totalsRow.innerHTML = "<td>Total</td>";

	columnSums.shift();
	rowSums.shift();
	data.shift();

	let sumOfSums = 0;

	for (let sum of columnSums) {
		let td = document.createElement("td");
		td.innerHTML = sum;
		sumOfSums += sum;
		totalsRow.appendChild(td);
	}

	let td = document.createElement("td");
	td.innerHTML = sumOfSums;
	totalsRow.appendChild(td);

	let expectedValues = [];

	for (let row in rowSums) {
		expectedValues[row] = [];

		for (let col in columnSums) {
			expectedValues[row][col] = rowSums[row] * columnSums[col] / sumOfSums;
		}

	}

	staticTbody.appendChild(totalsRow);

	let expValTbody = staticTbody.cloneNode(true);
	let chiTbody = staticTbody.cloneNode(true);
	let chiSum = 0;

	for (let i = 0; i < expectedValues.length; i++) {

		let exTr = expValTbody.children[i + 1];
		let chiTr = chiTbody.children[i + 1];

		for (let td = 1; td <= columnSums.length; td++) {
			let exTd = exTr.children[td];
			exTd.innerHTML = expectedValues[i][td - 1].toFixed(2);
			let chiTd = chiTr.children[td];

			let chi = ((expectedValues[i][td - 1] - data[i][td - 1]) ** 2) / expectedValues[i][td - 1];

			chiSum += chi;
			chiTd.innerHTML = chi.toFixed(2);
		}

	}

	for (let tr of chiTbody.children) {
		tr.children[tr.children.length - 1].remove();
	}

	totalChiEl.innerText = chiSum.toFixed(2);

	let degreesOfFreedom = (expectedValues.length - 1) * (expectedValues[0].length - 1);

	degFreeEl.innerText = degreesOfFreedom;

	pchiEl.innerText = ((1 - computeChi(chiSum, degreesOfFreedom)) * 100).toFixed(2);

	tblDiv.innerHTML = "";

	let staticTable = document.createElement("table");
	staticTable.appendChild(staticTbody);
	staticTable.style.display = "inline-block";

	tblDiv.appendChild(staticTable);

	expDiv.innerHTML = "";

	let expTable = document.createElement("table");
	expTable.appendChild(expValTbody);
	expTable.style.display = "inline-block";

	expDiv.appendChild(expTable);

	chiDiv.innerHTML = "";

	chiTbody.children[chiTbody.children.length - 1].remove();

	let chiTable = document.createElement("table");
	chiTable.appendChild(chiTbody);
	chiTable.style.display = "inline-block";

	chiDiv.appendChild(chiTable);

	myGraph = reboundGraph(myGraph, -1, 3 + 3 * degreesOfFreedom, Math.min(-.25 / degreesOfFreedom, -.025), Math.max(1 / degreesOfFreedom, .1))
	myGraph.shadeUnderCurve(chiSquarePDFFunc(degreesOfFreedom), Math.min(chiSum, myGraph.xMax), myGraph.xMax + 2);

	chiDataContainer.classList.add("hide");
	resDiv.classList.remove("hide");

};

function editData() {
	resDiv.classList.add("hide");
	chiDataContainer.classList.remove("hide");
};

function centerAddColCont() {
	addColContainer.style.marginTop = (table.clientHeight / 2 - addColBtn.clientHeight) + "px";
};

function centerAddRowCont() {
	//addRowContainer.style.width = table.clientWidth + "px";
};