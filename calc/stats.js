var normalTaylorSeries = "0";
for (var i = 1; i < 100; i++) {

	var numerator = "( (-1) ** ( 1 + " + i + ") ) * (x) ** (" + ((2 * i) - 1) + ")"
	var denom = "(" + ((2 * i) - 1) + ") * (" + (2 ** (i - 1)) + ") * ( " + factorial(i - 1) + " )";
	normalTaylorSeries += " + (" + numerator + ") / (" + denom + ")";

}

function normalTaylorApprox(lower, upper) {

	// we can't handle bounds of magnitude > 7... maximize and minimize.

	if (lower > 7) {
		lower = 7;
	}

	if (upper < -7) {
		upper = -7;
	}

	if (lower < -7) {
		lower = -7;
	}

	if (upper > 7) {
		upper = 7;
	}

	const constant = (1 / Math.sqrt(2 * Math.PI));

	var integral = eval(normalTaylorSeries.replaceAll("x", upper)) - eval(normalTaylorSeries.replaceAll("x", lower));

	integral *= constant;

	return integral;

}

function findz(percent, iter = 25) {
	let z = 0;
	let delta = 3;

	for (let i = 0; i < iter; i++) {

		let testPercent = normalTaylorApprox(-100, z);

		if (testPercent == percent) {
			return z;
		}

		if (testPercent * 100 < percent) {
			z += delta;
			delta /= 2;
		} else {
			z -= delta;
			delta /= 2;
		}

	}

	return z;

}

function factorial(n) {
	var product = 1;
	var multiplier = 1;
	while (multiplier <= n) {
		product *= multiplier;
		multiplier++;
	}

	return product;
}

function getLowerSummationGammaPrep(n, x) {
	let k = 0;
	let sum = 0;
	while (k < n) {

		sum += (x ** k) / factorial(k);

		k++;
	}
	return sum;
}

function lowerGammaFunction(n, x) {
	return gamma(n - 1) * (1 - ((Math.E ** -x) * getLowerSummationGammaPrep(n, x)));
}

function getNormalCurve(av, sd) {

	if (isNaN(av) || av === null) {
		av = 0;
		console.log("default average used.");
	}

	if (isNaN(sd) || sd === null || sd === 0 || sd < 0) {
		sd = 1;
		console.log("default sd used.");
	}

	return "(1 / ( " + sd + " * Math.sqrt(2 * Math.PI) )) * ( Math.E ** ( - ((x - " + av + ") ** 2) / ( 2 * (( " + sd + " ) ** 2)) ) )";
}

function getRegression(xList, yList, xVariable = "x") {
	let sdX = sd(xList);
	let sdY = sd(yList);

	let aveX = average(xList);
	let aveY = average(yList);

	let r = getr(xList, yList);

	if (sdX == 0) {
		return {
			display: "No Prediction",
			actual: "x * Infinity"
		};
	}

	let slope = r * sdY / sdX
	let slopeDisp = slope.toFixed(4);

	let yIntercept = aveY - aveX * slope;

	let sign = "+";

	if (yIntercept < 0) {
		sign = "-";
		yIntercept *= -1;
	}

	let yIntDisp = yIntercept.toFixed(4);

	return {
		display: `${slopeDisp} * (${xVariable}) ${sign} ${yIntDisp}`,
		actual: `${slope} * (${xVariable}) ${sign} ${yIntercept}`
	};
}

function gamma(k) {

	if (k % 1 == 0) {
		return factorial(k - 1);
	}

	var oneAndHalf = 0.88622692545275801364908374167057259139877472806119356410690389492645564229551609068747532836927233270811341181214;
	var half = oneAndHalf / 0.5;

	if (k == 0.5) {
		return half;
	}

	var product = oneAndHalf;

	k--;

	while (k >= 1.5) {
		product *= k;
		k--;
	}

	return product;

}

function getr(xList, yList) {

	if (xList.length != yList.length) {
		console.log("xList is not the same length as yList in getr function.");
		return;
	}

	let sdx = sd(xList);
	let sdy = sd(yList);
	let avex = average(xList);
	let avey = average(yList);

	if (sdx == 0 || sdy == 0) {
		return null;
	}

	let products = [];

	for (let i = 0; i < xList.length; i++) {

		let x = xList[i];
		let y = yList[i];

		let standardX = (x - avex) / sdx;
		let standardY = (y - avey) / sdy;

		products.push(standardX * standardY);

	}

	return average(products);

}

function average(list) {

	let sum = 0;

	for (let val of list) {
		sum += val;
	}

	return sum / list.length;

}

function variance(list) {
	// get the average
	var xbar = average(list);

	// set up a summation for the sd calculation
	var summation = 0;

	for (var val of list) {
		summation += (val - xbar) ** 2;
	}

	return summation / list.length;

}

function sd(list) {

	let varianceOfList = variance(list);

	// get the sd
	return Math.sqrt(varianceOfList);
}

function sdPlus(list) {
	let listSD = sd(list);
	let listSDPlus = Math.sqrt(list.length / (list.length - 1)) * listSD;
	return listSDPlus;
}

// function to grab a random int between two numbers. For best results, min and max should be integers.
function randomInt(min, max) {
	return Math.floor(min + Math.random() * (max - min + 1));
}

function integrate(inputFx, lower, upper) {
	var integral = 0;

	if (inputFx == getNormalCurve()) {
		// use some shortcuts

		// lower half is .5
		if (lower <= -5) {
			integral += .5;
			lower = 0;
			if (upper == 0) {
				return integral;
			}
		}

		// upper half is .5
		if (upper >= 5) {
			integral += .5;
			upper = 0;
			if (lower == 0) {
				return integral;
			}
		}

		return integral + normalTaylorApprox(lower, upper);

	}

	var delta = (upper - lower) / 10000;

	var x = lower;

	var mid;

	while (x < upper) {
		mid = x + (delta / 2);
		integral += eval(inputFx.replaceAll("x", mid)) * delta;
		x += delta;
	}

	return integral;
}