const tbody = document.getElementById("lists").children[0];
const letters = ["X", "Y", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "Z"];
const error = document.getElementById("error");

const stats = document.getElementById("stats");
const aveEl = document.getElementById("average");
const sdEl = document.getElementById("sd");
const sdPlusEl = document.getElementById("sdp");
const statsSelectBox = document.getElementById("statsSelectBox");

let inverted = true;

function addRow() {
	let rows = tbody.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];

	let newRow = lastRow.cloneNode(true);

	for (let inp of newRow.getElementsByTagName("input")) {
		inp.value = "";
	}

	for (let spn of newRow.getElementsByTagName("span")) {
		spn.textContent = "";
	}

	tbody.appendChild(newRow);
}

function removeRow() {
	let rows = tbody.getElementsByTagName("tr");

	let lastRow = rows[rows.length - 1];

	if (rows.length < 3) {
		alert("You cannot remove the only row of data.");
		return;
	} else if (
		lastRow.getElementsByTagName("input")[0].value.length < 1
		|| confirm("Would you like to remove the last row of data?")
	) {

		lastRow.remove();
	}

}

function calculateListValues() {
	let variables = [];

	let dependencies = [];
	let functions = [];

	for (let i = 0; i < tbody.children[0].children.length; i++) {
		variables[i] = letters[i];
		dependencies.push([]);
		functions.push("");
	}

	let firstRowTds = tbody.children[0].children;
	let plainData = [];

	for (let i = 0; i < firstRowTds.length; i++) {
		let td = firstRowTds[i];

		if (!td.getElementsByClassName("functionCheckBox")[0].checked) {
			plainData.push(i);
			continue;
		}

		let funcInp = td.getElementsByClassName("functionInput")[0];

		// remove c's, d's, and a's so it doesn't have weird dependencies.
		let func = (funcInp.value + "").toUpperCase().replaceAll("COL", "OL").replaceAll("COUNT", "OUNT").replaceAll("SD", "S").replaceAll("AVG", "VG");

		functions[i] = func;

		for (let k = 0; k < variables.length; k++) {
			let variable = variables[k];
			if (func.toUpperCase().indexOf(variable) > -1) {

				if (i == k) {
					error.textContent = `The formula in Column ${letters[i]} contains ${letters[i]} itself. Please adjust the formula.`;
					return;
				}

				dependencies[i].push(k);
			}
		}
	}

	let orderedCalc = [];
	let uncalculated = functions.length;

	while (uncalculated > 0) {

		let needPossibleCalculation = true;

		for (let j = 0; j < dependencies.length; j++) {
			if (dependencies[j].length == 0 && orderedCalc.indexOf(j) == -1) {
				// no more dependencies. It can be calculated!
				needPossibleCalculation = false;

				orderedCalc.push(j);

				for (let k = 0; k < dependencies.length; k++) {
					dependencies[k].remove(j);
				}

				uncalculated--;

				// no need to find any other variables.
				break;
			}
		}

		if (needPossibleCalculation) {

			let columns = "";

			for (let j = 0; j < dependencies.length; j++) {
				if (dependencies[j].length > 0 && dependencies[j][0] != "Completed") {
					if (columns.length > 0) {
						columns += `, ${letters[j]}`;
					} else {
						columns += `${letters[j]}`;
					}
				}
			}

			error.textContent = `Columns ${columns} contain a loop (i.e. A = B, B = C, and C = A). Please adjust these formulas to remove the loop.`;

			return;
		}

	}


	// C is a variable itself. Thus, COL becomes OL and COUNT becomes OUNT
	let permVariables = ["ROW", "OL", "OUNT"];

	let values = [];

	for (let toCalc of orderedCalc) {

		for (let row = 1; row < tbody.children.length; row++) {
			let tr = tbody.children[row];

			if (values[row] == undefined) {
				values[row] = [];
			}

			if (plainData.indexOf(toCalc) > -1) {
				values[row][toCalc] = tr.children[toCalc].getElementsByTagName("input")[0].value * 1;
			} else {

				if (functions[toCalc].length < 1) {
					values[row][toCalc] = 0;
				} else {

					while (functions[toCalc].indexOf("VG") > -1) {

						let match = functions[toCalc].matchAll(/VG\([A-Fa-fXYxy]\)/g).next();

						if (!match.done) {
							let index = match.value.index + 3;
							let columnTitle = functions[toCalc][index].toUpperCase();
							let column = letters.indexOf(columnTitle);

							let columnData = getData(column);

							let avg = average(columnData);

							functions[toCalc] = functions[toCalc].replaceAll(/VG\([A-Fa-fXYxy]\)/g, avg);

						} else {
							error.textContent = `Your statment for calculating an average in column ${letters[toCalc]} is formed incorrectly. An example of a properly formed statement is avg(B).`;
							return;
						}

					}

					while (functions[toCalc].indexOf("S") > -1) {

						let match = functions[toCalc].matchAll(/S\([A-Fa-fXYxy]\)/g).next();

						if (!match.done) {
							let index = match.value.index + 2;
							let columnTitle = functions[toCalc][index].toUpperCase();
							let column = letters.indexOf(columnTitle);

							let columnData = getData(column);

							let colSD = sd(columnData);

							functions[toCalc] = functions[toCalc].replaceAll(/S\([A-Fa-fXYxy]\)/g, colSD);

						} else {
							error.textContent = `Your statment for calculating a standard deviation in column ${letters[toCalc]} is formed incorrectly. An example of a properly formed statement is sd(B).`;
							return;
						}

					}

					let equation = insertVariables(functions[toCalc], permVariables, [row, toCalc + 1, row]).replaceAll(/\^/g, "**");
					equation = insertVariables(equation, variables, values[row]).replaceAll(/\^/g, "**");
					values[row][toCalc] = eval(equation);
				}

				tr.children[toCalc].getElementsByTagName("input")[0].value = values[row][toCalc];
				tr.children[toCalc].getElementsByTagName("span")[0].textContent = Math.round(values[row][toCalc] * 1000) / 1000;
			}

		}

	}

	error.textContent = "";

}

function showFunctionInput(checkbox) {
	let functionInput = checkbox.parentElement.getElementsByClassName("functionInput")[0];

	let column = checkbox.getAttribute("data-column");

	if (checkbox.checked) {

		functionInput.classList.remove("hide");

		for (let tr of tbody.children) {

			let colRowInp = tr.children[column].getElementsByTagName("input")[0];

			if (colRowInp.getAttribute("type") != "checkbox") {
				colRowInp.classList.add("hide");
				tr.children[column].getElementsByTagName("span")[0].classList.remove("hide");
			}

		}

	} else {

		functionInput.classList.add("hide");

		for (let tr of tbody.children) {

			let colRowInp = tr.children[column].getElementsByTagName("input")[0];

			if (colRowInp.getAttribute("type") != "checkbox") {
				colRowInp.classList.remove("hide");
				tr.children[column].getElementsByTagName("span")[0].classList.add("hide");
			}

		}


	}

}

function showStats() {
	updateStatsDisplay();
	stats.classList.add("fullscreen");
	stats.classList.remove("hide");
}

function hideStats() {
	stats.classList.add("hide");
	stats.classList.remove("fullscreen");
}

function updateStatsDisplay() {
	let column = statsSelectBox.value;

	if (column.length < 1) {
		return;
	}

	let dataList = getData(column);

	aveEl.textContent = average(dataList).toFixed(4);
	sdEl.textContent = sd(dataList).toFixed(4);
	sdPlusEl.textContent = sdPlus(dataList).toFixed(4);
}

function getData(column) {
	let dataList = [];
	let useSpan = null;

	for (let tr of tbody.children) {
		let td = tr.children[column];
		let checkbox = td.getElementsByClassName('functionCheckBox');

		if (checkbox.length > 0) {
			useSpan = checkbox[0].checked;
		} else {

			if (useSpan) {
				dataList.push(td.getElementsByTagName("span")[0].textContent * 1);
			} else {
				dataList.push(td.getElementsByTagName("input")[0].value * 1);
			}

		}

	}

	return dataList;

}