/*
	LogGamma(Z)

	This function is Dr. Ferguson's work.
*/
function LogGamma(Z) {
	// Binet's Log Gamma Formulas? This function is Dr. Ferguson's work.
	with (Math) {
		var S = 1 + 76.18009173 / Z - 86.50532033 / (Z + 1) + 24.01409822 / (Z + 2) - 1.231739516 / (Z + 3) + .00120858003 / (Z + 4) - .00000536382 / (Z + 5);
		var LG = (Z - .5) * log(Z + 4.5) - (Z + 4.5) + log(S * 2.50662827465);
	}
	return LG;
}

/*
	gammaConvert(x)
	takes the result of LogGamma(Z) as input
	outputs the value of the Gamma distribution at Z

	NOTE: Math.exp raises e to the power of the parameter by default.
	NOTE: Default base of log in Javascript is the natural log; LogGamma is same as lnGamma here.
*/
function gammaConvert(x) {
	// e to the power of the natural log of the result of the gamma distribution
	// is just the result of the gamma distribution.
	return Math.exp(LogGamma(x));
}

/*
	Gcf(X,A)

	This function is Dr. Ferguson's work.

	It is a cdf for the gamma function with a scale parameter of 2.

	It is valid for for X>A+1
*/
function Gcf(X, A) {
	with (Math) {
		var A0 = 0;
		var B0 = 1;
		var A1 = 1;
		var B1 = X;
		var AOLD = 0;
		var N = 0;
		while (abs((A1 - AOLD) / A1) > .00001) {
			AOLD = A1;
			N = N + 1;
			A0 = A1 + (N - A) * A0;
			B0 = B1 + (N - A) * B0;
			A1 = X * A0 + N * A1;
			B1 = X * B0 + N * B1;
			A0 = A0 / B1;
			B0 = B0 / B1;
			A1 = A1 / B1;
			B1 = 1;
		}
		var Prob = exp(A * log(X) - X - LogGamma(A)) * A1;
	}
	return 1 - Prob;
}

/*
	Gser(X,A)

	This function is Dr. Ferguson's work.

	It is a cdf for the gamma function with a scale parameter of 2.

	It is valid for X<A+1.

*/
function Gser(X, A) {
	with (Math) {
		var T9 = 1 / A;
		var G = T9;
		var I = 1;
		while (T9 > G * .00001) {
			T9 = T9 * X / (A + I);
			G = G + T9;
			I = I + 1;
		}
		G = G * exp(A * log(X) - X - LogGamma(A));
	}
	return G;
}

/*
	Gammacdf(x,a)

	This function is Dr. Ferguson's work.

*/
function Gammacdf(x, a) {
	var GI;
	if (x <= 0) {
		GI = 0;
	} else if (x < a + 1) {
		GI = Gser(x, a);
	} else {
		GI = Gcf(x, a);
	}
	return GI;
}

/*
	computeChi(z, v)

	This function is an adapted version of Dr. Ferguson's work.
	It represents the value of the Chi Squared CDF at z and v.

	Parameters:
		- z: test statistic
		- v: degrees of freedom

	Output:
		- the value of the Chi Squared Distribution at the given x with v degrees of freedom

*/
function computeChi(z, v) {
	// make sure z and v are numbers
	Z = eval(z);
	DF = eval(v);

	// Do we have non-positive degrees of freedom?
	if (DF <= 0) {
		// yes, the degrees of freedom are invalid.
		// return null to show an error.
		return null;
	} else {
		// no, the degrees of freedom are valid.
		// get the value of the chi squared cdf
		Chisqcdf = Gammacdf(Z / 2, DF / 2);
	}

	// round the result and return it.
	Chisqcdf = Math.round(Chisqcdf * 100000) / 100000;
	return Chisqcdf;
}

/*
	chiSquarePDF(x, k)

	Parameters:
		- x: a test statistic or x value
		- k: degrees of freedom in the chi squared distribution.

	Output:
		- the value of the chi squared pdf at the given x with k degrees of freedom.

*/
function chiSquarePDF(x, k) {

	// Negative values on the chi squared distribution are all 0
	if (x <= 0) {
		return 0;
	}

	// put together the equation we want to evalute
	// the equation was transcribed into Javascript from Wikepedia
	let chiPDF = "( ( x ** (k/2-1) ) * Math.exp(-1 * x / 2) ) / ( (2 ** (k/2)) * gammaConvert(k/2) )";

	// evaluate the function at the given degrees of freedom (k)
	// and the given test statistic (x)
	chiPDF = chiPDF.replaceAll("k", k).replaceAll(" x ", x);
	return eval(chiPDF);

}

/*
	chiSquarePDFFunc(degFree)

	Parameters:
		- degFree: integer input. Degrees of Freedom in the chi squared distribution.

	Output:
		- a string that represents the chi squared pdf.
			This string can be used to graph the distribution.
*/
function chiSquarePDFFunc(degFree) {
	return `chiSquarePDF(x, ${degFree})`;
}