var canvas = document.getElementById("graph1");
var context = canvas.getContext("2d");
var myGraph = setUpGraph(canvas, context, -3, 3, -0.5, 0.5, 500, 500);

const typeSelector = document.getElementById("testType");
const runBtn = document.getElementById("runBtn");
const result = document.getElementById("result");

// set up the required controls
for (var reqControl of document.getElementsByClassName("required")) {
	reqControl.onkeyup = (ev) => {
		runBtn.classList.add("hide");
		for (var reqControl of document.querySelectorAll(".control-section:not(.hide) .required")) {
			if (reqControl.value.length < 1) {
				// don't enable button
				document.getElementById("runBtn").disabled = true;
				return;
			}
		}
		// all visable controls have values... let it run!
		document.getElementById("runBtn").disabled = false;

		test = ev;

		myGraph.clearGraph();
		result.innerText = "";

		// make it run if they hit enter
		if (ev.keyCode == 13) {
			run();
			return;
		}

		runBtn.classList.remove("hide");

	};

	reqControl.onchange = reqControl.onkeyup;
}

function showControls() {
	runBtn.classList.add("hide");
	result.innerText = "";
	myGraph.clearGraph();


	// hide the control sections we don't need
	for (var sec of document.getElementsByClassName("control-section")) {
		sec.classList.add("hide");
	}

	// clear the controls
	for (var reqControl of document.getElementsByClassName("required")) {
		reqControl.value = "";
	}

	// don't let them run it until it is ready
	document.getElementById("runBtn").disabled = true;

	// display the control section we do need
	document.getElementById(typeSelector.value + "-controls").classList.remove("hide")
}

function run() {

	runBtn.classList.add("hide");

	switch (typeSelector.value) {
		case "normal":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)

			var upper = parseFloat(document.getElementById("normal-upper").value);
			var lower = parseFloat(document.getElementById("normal-lower").value);

			if (upper < lower) {
				console.log(upper + "<" + lower);
				console.log(Number(upper) + "<" + Number(lower));
				console.log(upper < lower);
				result.innerText = "ERROR: Lower bound is greater than upper bound.";
				return;
			} else if (upper == lower) {
				result.innerText = "ERROR: Lower bound is equal to upper bound (area is 0)";
			}

			myGraph.shadeUnderCurve(getNormalCurve(), lower, upper);

			result.innerText = "p = " + ((integrate(getNormalCurve(), lower, upper) * 100).toFixed(2)) + "%";
			break;

		case "anormal":

			var upper = parseFloat(document.getElementById("anormal-upper").value);
			var lower = parseFloat(document.getElementById("anormal-lower").value);
			let aNormalSD = parseFloat(document.getElementById("anormal-sd").value);
			let aNormalAvg = parseFloat(document.getElementById("anormal-avg").value);

			let standardUpper = (upper - aNormalAvg) / aNormalSD;
			let standardLower = (lower - aNormalAvg) / aNormalSD;

			result.innerText = "p = " + ((integrate(getNormalCurve(), standardLower, standardUpper) * 100).toFixed(2)) + "%";

			let leftBound = -3 * aNormalSD + aNormalAvg;
			let rightBound = 3 * aNormalSD + aNormalAvg;
			let height = .5 / aNormalSD;
			let lowerBound = -.25 / aNormalSD;

			myGraph = reboundGraph(myGraph, leftBound, rightBound, lowerBound, height)

			myGraph.shadeUnderCurve(getNormalCurve(aNormalAvg, aNormalSD), lower, upper);

			break;

		case "normal1":

			var dir = document.getElementById("normal1-dir").value;
			var z = parseFloat(document.getElementById("normal1-z").value);
			var lower = 0;
			var upper = 0;

			if (z < 3 && z > -3) {
				myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)
			} else if (z < 0) {
				myGraph = reboundGraph(myGraph, z - 1, 3, -.25, .5)
			} else if (z > 0) {
				myGraph = reboundGraph(myGraph, -3, z + 1, -.25, .5)
			}

			if (dir == "above") {
				lower = z;
				upper = 100;
			} else {
				upper = z;
				lower = -100;
			}

			myGraph.shadeUnderCurve(getNormalCurve(), Math.max(lower, myGraph.xMin), Math.min(upper, myGraph.xMax));

			result.innerText = "p = " + ((integrate(getNormalCurve(), lower, upper) * 100).toFixed(2)) + "%";

			break;

		case "normal2":

			var z = parseFloat(document.getElementById("normal2").value);
			var lower = 0;
			var upper = 0;


			if (z > 0) {
				upper = z;
				lower = -1 * z;
			} else {
				lower = z;
				upper = -1 * z;
			}

			if (upper < 3) {
				myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)
			} else {
				myGraph = reboundGraph(myGraph, lower - 1, upper + 1, -.25, .5)
			}

			myGraph.shadeUnderCurve(getNormalCurve(), lower, upper, true);

			if (upper > 5) {
				result.innerText = "p = " + ((0).toFixed(2)) + "%";
			} else {
				result.innerText = "p = " + ((integrate(getNormalCurve(), -100, lower) * 200).toFixed(2)) + "%";
			}

			break;

		case "2sampz1":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5);

			let onetailDirection = document.getElementById("shade1tail").value;
			let statTypeonetail = document.getElementById("stattype1tail").value;

			let obsFactoronetail = statTypeToObsFactor(statTypeonetail);

			let obs1onetail = document.getElementById("obs11tail").value * obsFactoronetail;
			let obs2onetail = document.getElementById("obs21tail").value * obsFactoronetail;
			let sd1onetail = document.getElementById("sd11tail").value;
			let sd2onetail = document.getElementById("sd21tail").value;
			let n1onetail = document.getElementById("n11tail").value;
			let n2onetail = document.getElementById("n21tail").value;

			let twosampzonetail = (obs1onetail - obs2onetail) / (Math.sqrt((sd1onetail ** 2 / n1onetail) + (sd2onetail ** 2 / n2onetail)));

			if (onetailDirection == "above") {
				lower = twosampzonetail;
				upper = 10;
			} else {
				upper = twosampzonetail;
				lower = -10;
			}

			myGraph.shadeUnderCurve(getNormalCurve(), lower, upper);

			if (lower > 5 || upper < -5) {
				result.innerHTML = "z = " + twosampzonetail + "<br>p = " + ((0).toFixed(2)) + "%";
			} else {
				result.innerHTML = "z = " + twosampzonetail + "<br>p = " + ((integrate(getNormalCurve(), lower, upper) * 100).toFixed(2)) + "%";
			}

			break;

		case "2sampz2":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5);

			let statType = document.getElementById("stattype2tail").value;

			let obsFactor = statTypeToObsFactor(statType);

			let obs1 = document.getElementById("obs12tail").value * obsFactor;
			let obs2 = document.getElementById("obs22tail").value * obsFactor;
			let sd1 = document.getElementById("sd12tail").value;
			let sd2 = document.getElementById("sd22tail").value;
			let n1 = document.getElementById("n12tail").value;
			let n2 = document.getElementById("n22tail").value;

			let twosampz = (obs1 - obs2) / (Math.sqrt((sd1 ** 2 / n1) + (sd2 ** 2 / n2)));

			if (twosampz > 0) {
				upper = twosampz;
				lower = -1 * twosampz;
			} else {
				lower = twosampz;
				upper = -1 * twosampz;
			}

			myGraph.shadeUnderCurve(getNormalCurve(), lower, upper, true);

			if (upper > 5) {
				result.innerHTML = "z = " + twosampz + "<br>p = " + ((0).toFixed(2)) + "%";
			} else {
				result.innerHTML = "z = " + twosampz + "<br>p = " + ((integrate(getNormalCurve(), -100, lower) * 200).toFixed(2)) + "%";
			}

			break;

		case "t":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5);

			var upper = parseFloat(document.getElementById("t-upper").value);
			var lower = parseFloat(document.getElementById("t-lower").value);
			var degFree = parseFloat(document.getElementById("t-deg").value);

			if (upper < lower) {
				console.log(upper + "<" + lower);
				console.log(Number(upper) + "<" + Number(lower));
				console.log(upper < lower);
				result.innerText = "ERROR: Lower bound is greater than upper bound.";
				return;
			} else if (upper == lower) {
				result.innerText = "ERROR: Lower bound is equal to upper bound (area is 0)";
			} else if (degFree < 1) {
				result.innerText = "ERROR: Degrees of freedom must be positive";
			}

			myGraph.shadeUnderCurve(tPDFFunc(degFree), lower, upper);

			result.innerText = "p = " + (((tCDF(upper, degFree) - tCDF(lower, degFree)) * 100).toFixed(2)) + "%";
			break;

		case "t1":

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)

			var dir = document.getElementById("t1-dir").value;
			var t = parseFloat(document.getElementById("t1-t").value);
			var degFree = parseFloat(document.getElementById("t1-deg").value);
			var lower = 0;
			var upper = 0;

			if (t < 3 && t > -3) {
				myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)
			} else if (t < 0) {
				myGraph = reboundGraph(myGraph, t - 1, 3, -.25, .5)
			} else if (t > 0) {
				myGraph = reboundGraph(myGraph, -3, t + 1, -.25, .5)
			}

			if (dir == "above") {
				lower = t;
				upper = 1000;
			} else {
				upper = t;
				lower = -1000;
			}

			myGraph.shadeUnderCurve(tPDFFunc(degFree), lower, upper);

			result.innerText = "p = " + (((tCDF(upper, degFree) - tCDF(lower, degFree)) * 100).toFixed(2)) + "%";

			break;

		case "t2":

			var t = parseFloat(document.getElementById("t2").value);
			var degFree = parseFloat(document.getElementById("t2-deg").value);

			myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)

			var lower = 0;
			var upper = 0;


			if (t > 0) {
				upper = t;
				lower = -1 * t;
			} else {
				lower = t;
				upper = -1 * t;
			}

			if (upper < 3) {
				myGraph = reboundGraph(myGraph, -3, 3, -.25, .5)
			} else {
				myGraph = reboundGraph(myGraph, lower - 1, upper + 1, -.25, .5)
			}

			myGraph.shadeUnderCurve(tPDFFunc(degFree), lower, upper, true);

			result.innerText = "p = " + ((tCDF(lower, degFree) * 200).toFixed(2)) + "%";


			break;

		case "chiSquare":

			var lower = parseFloat(document.getElementById("chi-lower").value);
			var degFree = parseFloat(document.getElementById("chi-deg").value);

			myGraph = reboundGraph(myGraph, -1, 3 + 3 * degFree, Math.min(-.25 / degFree, -.025), Math.max(1 / degFree, .1))

			myGraph.shadeUnderCurve(chiSquarePDFFunc(degFree), Math.min(lower, myGraph.xMax), myGraph.xMax + 2);

			result.innerText = "p = " + (((1 - computeChi(lower, degFree)) * 100).toFixed(2)) + "%";
			break;

		default:
			console.log("ERROR: Operation not found");
	}

}

function statTypeToObsFactor(statType) {
	if (statType == "percent") {
		return .01;
	} else {
		return 1;
	}
}