function initTable(target, data = [[0, 0], [0, 0]]) {
	let tableObj = {};
	tableObj.tgt = target;
	tableObj.prt = document.getElementById(tableObj.tgt);
	tableObj.dta = data;

	tableObj.createInput = function (row, column, table, val = 0) {
		let ip = document.createElement("input");
		ip.value = val;

		ip.setAttribute("data-ri", row);
		ip.setAttribute("data-ci", column);

		ip.onkeyup = function (event) {
			table.updateData(ip, event.keyCode);
		}

		return ip;
	}

	tableObj.build = function () {
		let table = document.createElement("table");

		this.tbl = table;

		let rowNum = 0;
		let columnNum = 0;

		let rowLength = 0;

		// repeat twice the following for loop
		for (let i = 0; i < 2; i++)
			for (let row of data) {

				if (rowLength < row.length) {
					rowLength = row.length;
				}

				while (rowLength > row.length) {
					row.push(0);
				}

			}

		this.col = switchRowsForColumns(data);

		this.rln = rowLength;
		this.cln = data.length;

		for (let row of data) {

			let tr = document.createElement("tr");

			for (let cell of row) {

				let td = document.createElement("td");
				let ip = this.createInput(rowNum, columnNum, this, cell);

				td.appendChild(ip);
				tr.appendChild(td);

				columnNum++;

			}

			columnNum = 0;
			rowNum++;

			table.appendChild(tr);

		}

		tableObj.container = document.createElement("div");

		tableObj.container.style = "display:flex;";

		tableObj.container.appendChild(table);

		let columnPlus = document.createElement("div");

		columnPlus.style = "font-size: 30px;margin: auto; margin-left: 5px; height: 35px; background-color: lightgrey; border-radius: 15px;cursor:pointer;";
		columnPlus.innerText = "+";

		let tableObjPointer = this;

		columnPlus.onclick = function () { tableObjPointer.addColumn(); };

		tableObj.container.appendChild(columnPlus);

		let rowPlus = document.createElement("div");

		rowPlus.style = "font-size: 30px;margin-left: auto; height: 35px; background-color: lightgrey; border-radius: 15px;cursor:pointer; text-align:center;";
		rowPlus.innerText = "+";

		rowPlus.onclick = function () { tableObjPointer.addRow(); };



		this.prt.appendChild(tableObj.container);

		tableObj.prt.appendChild(rowPlus);

	}

	tableObj.updateData = function (ip, keyCode) {

		let newVal = parseInt(ip.value);

		if (isNaN(newVal)) {
			newVal = 0;
		}

		ip.value = newVal;
		let row = ip.getAttribute("data-ri");
		let clm = ip.getAttribute("data-ci");

		this.dta[row][clm] = newVal;
		this.col[clm][row] = newVal;

		row++;

		if (keyCode == 13 && row < this.tbl.children.length) {
			let newCell = this.tbl.children[row].children[clm].children[0];
			newCell.focus();
		} else if (keyCode == 13) {
			this.addRow();
			let newCell = this.tbl.children[row].children[clm].children[0];
			newCell.focus();
		}

	}

	tableObj.addRow = function () {

		let tr = document.createElement("tr");
		let dataRow = [];

		for (let i = 0; i < this.rln; i++) {

			this.col[i].push(0);

			dataRow.push(0);

			let td = document.createElement("td");

			let ip = this.createInput(this.cln, i, this);

			td.appendChild(ip);
			tr.appendChild(td);
		}

		this.dta.push(dataRow);

		this.tbl.appendChild(tr);

		this.cln++;

	}

	tableObj.addColumn = function () {

		let dataCol = [];

		for (let i = 0; i < this.tbl.children.length; i++) {

			dataCol.push(0);

			this.dta[i].push(0);

			let td = document.createElement("td");

			let ip = this.createInput(i, this.rln, this);

			td.appendChild(ip);

			this.tbl.children[i].appendChild(td);
		}

		this.col.push(dataCol);

		this.rln++;

	}

	return tableObj;
}

function switchRowsForColumns(data) {
	let newData = [];

	for (let row in data) {
		for (let col in data[row]) {

			if (typeof newData[col] !== "object") {
				newData.push([]);
			}

			newData[col][row] = data[row][col]
		}
	}

	return newData;
}